package com.stygian.jira.plugins.jql;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.NotNull;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import com.google.common.collect.Iterables;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Echoes the the string passed in as an argument.
 */
public class DateRangeJqlFunction extends AbstractJqlFunction
{
    private static final Logger log = LoggerFactory.getLogger(DateRangeJqlFunction.class);

    public MessageSet validate(User searcher, FunctionOperand operand, TerminalClause terminalClause)
    {
        return validateNumberOfArgs(operand, 3);
    }

    public List<QueryLiteral> getValues(QueryCreationContext queryCreationContext, FunctionOperand operand, TerminalClause terminalClause)
    {
        final List<QueryLiteral> literals = new LinkedList<QueryLiteral>();
        try
        {
            Project project = ComponentAccessor.getProjectManager().getProjectObjByKey(operand.getArgs().get(0));
            Integer startDay = Integer.parseInt(operand.getArgs().get(1));
            Integer endDay = Integer.parseInt(operand.getArgs().get(2));
            if(startDay > endDay)
            {
                throw new Exception("Start day cannot be greater than end day");
            }
            if(startDay < 1 || startDay > 31 || endDay < 1 || endDay > 31)
            {
                throw new Exception("Days should be between 1 and 31.");
            }
            Collection<Long> issueIds = ComponentAccessor.getIssueManager().getIssueIdsForProject(project.getId());
            for(Long issueId:issueIds)
            {
                Issue issue = ComponentAccessor.getIssueManager().getIssueObject(issueId);
                DateTime issueUpdated = new DateTime(issue.getUpdated());
                if(issueUpdated.getDayOfMonth() > startDay && issueUpdated.getDayOfMonth() < endDay)
                {
                    literals.add(new QueryLiteral(operand, issueId));
                }

            }
        }
        catch(Exception exc)
        {
            log.error(exc.toString());
        }
        return literals;
    }

    public int getMinimumNumberOfExpectedArguments()
    {
        return 3;
    }

    public JiraDataType getDataType()
    {
        return JiraDataTypes.ISSUE;
    }
}