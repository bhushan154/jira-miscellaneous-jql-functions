package com.stygian.jira.plugins.jql;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: bhushan
 * Date: 11/11/13
 * Time: 10:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class ComponentsWithNoLeads  extends AbstractJqlFunction {
    private static final Logger log = LoggerFactory.getLogger(DateRangeJqlFunction.class);

    public MessageSet validate(User searcher, FunctionOperand operand, TerminalClause terminalClause)
    {
        return validateNumberOfArgs(operand, 0);
    }

    public List<QueryLiteral> getValues(QueryCreationContext queryCreationContext, FunctionOperand operand, TerminalClause terminalClause)
    {
        final List<QueryLiteral> literals = new LinkedList<QueryLiteral>();
        Collection<Long> issueIds = new ArrayList<Long>();
        try
        {
            List<Project> projects = ComponentAccessor.getProjectManager().getProjectObjects();
            for(Project project:projects){
                Collection<ProjectComponent> components = project.getProjectComponents();
                for(ProjectComponent component:components){
                    if(component.getLead() == null){
                        issueIds = ComponentAccessor.getProjectComponentManager().getIssueIdsWithComponent(component);
                    }
                }
            }
            for(Long issueId:issueIds)
            {
                literals.add(new QueryLiteral(operand, issueId));
            }
        }
        catch(Exception exc)
        {
            log.error(exc.toString());
        }
        return literals;
    }

    public int getMinimumNumberOfExpectedArguments()
    {
        return 0;
    }

    public JiraDataType getDataType()
    {
        return JiraDataTypes.ISSUE;
    }
}
